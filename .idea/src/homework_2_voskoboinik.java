import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static java.lang.System.out;


public class homework_2_voskoboinik {


    public static void main(String[] args) throws Exception {
        // int q = 44, w = 51, e = 154;
        //System.out.println("Min is " + minOfThree(q, w, e));
        //factorial();
        // fact();
        //int result = factorial(7);
        //System.out.println(result);
        // arr();
        // array();
        // arithmetic(15,3);
        // minandmaxonearr();
       //minandmaxtwoarr();
        // diagonal();
        // month();

    }

    public static int minOfThree(int a, int b, int c) throws Exception {
        int result;
        if (a < b && a < c) {
            result = a;
        } else if (c < b && c < a) {
            result = c;
        } else if (b < c && b < a) {
            result = b;
        } else {
            result = 0;
            out.println(" no  min Of Three!");
        }
        return result;
    }

    public static void factorial() throws Exception {
        {

            int number = 5;
            int res = 1;

            while (number > 0) {
                res = res * number;
                number--;
            }
            out.println("Factorial = " + res);
        }

    }

    public static void fact() throws Exception {
        int a = 3, fact = 1, b = 1;
        do {
            fact = fact * b;
            out.println("Factorial = " + fact);
            b++;
        }
        while (a >= b);

    }

    public static int factorial(int n) throws Exception {
        int x = 1;
        int y = 1;
        for (int i = 1; i <= n; i++) {

            y = x * i;
            x = y;

        }
        return y;
    }

    public static void arr() throws Exception {
        int[] a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};

        for (int i = 0; i < 10; i++) {

            out.println(a[i]);
        }
    }

    public static void array() throws Exception {
        int[] a = {4, 5, 6, 2, 5, 7, 8, 5};


        for (int i = 0; i < a.length; i++) {

            if (a[i] != 5)

                out.println(a[i]);
        }
    }

    public static void arithmetic(int a, int b) throws IOException {


        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String s = reader.readLine();
        switch (s) {
            case "/":
                out.println("A/B=" + a / b);
                break;
            case "*":
                out.println("A*B=" + a * b);
                break;
            case "+":
                out.println("A+B=" + (a + b));
                break;
            case "-":
                out.println("A-B=" + (a - b));
                break;
            default:
                out.println("Operation error");
        }

    }

    public static void minandmaxonearr() throws Exception {
        int[] array = {12, 1, 5, 7, 13, -1, -5};
        int max = array[0], min = array[0];
        for (int i = 0; i < array.length; i++) {
            if (max < array[i])
                max = array[i];
            if (min > array[i])
                min = array[i];
        }
        out.println("Max: " + max);
        out.println("Min:" + min);

    }

    public static void minandmaxtwoarr () throws Exception {

        int [][] mas = new int [4][6];
        int min = mas[0][0];
        int max = mas[0][0];
        int sum = 0;
        for(int i = 0; i<mas.length; i++){
            for(int j= 0; j<mas[0].length; j++){
                if(min>mas[i][j]){
                    min = mas[i][j];
                }
                if(max<mas[i][j]){
                    max = mas[i][j];
                }
                sum+=mas[i][j];
            }

            out.print("Сумма строки № "+i+" "+sum+"\n ");
            out.print("Наибольшее число строки "+i+" "+max+" \n");
            out.print("Наименьшее число строки "+i+" "+min+" \n\n");
            max = min = sum = 0;


    }}

    public static void diagonal()throws Exception{
        out.println("Array:");


        int arr[][]= new int[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                arr[i][j] = 2*i + j;
                out.print(arr[i][j]+" ");
            }
            out.println(" ");
        }


        int summ=0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if(i<=j) break;
                else summ+=arr[i][j];
            }
        }
        out.println("");
        out.println("Summ under main diagonal: "+summ);
    }
    public static void month()throws Exception {


        int arr[] = new int[31];
        for (int i = 0; i < 31; i++) {
            arr[i] = i + 1;
        }

        for (int i = 0; i < 31; i++) {
            if (((arr[i] % 7) - 5) == 0) out.println(arr[i] + " day is Friday");
        }

        out.print("31 day is ");
        switch (arr[30] % 7) {
            case (0):
                out.println("Sunday");
                break;
            case (1):
                out.println("Monday");
                break;
            case (2):
                out.println("Tuesday");
                break;
            case (3):
                out.println("Wednesday");
                break;
            case (4):
                out.println("Thursday");
                break;
            case (5):
                out.println("Friday");
                break;
            default:
                out.println("Saturday");
        }
    }
}

